<?xml version='1.0' encoding='utf-8'?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
    <title>Filter nbsp</title>
    <short_name>filter_nbsp</short_name>

    <api_version>7.x</api_version>

    <recommended_major>1</recommended_major>
    <supported_majors>1</supported_majors>
    <default_major>1</default_major>

    <project_status>published</project_status>

    <link>https://bitbucket.org/hamuchen/filter_nbsp</link>

    <terms>
        <term>
            <name>Projects</name>
            <value>Modules</value>
        </term>
    </terms>

    <releases>
        <release>
            <name>filter_nbsp 7.x-1.0</name>
            <version>7.x-1.0</version>

            <version_major>1</version_major>
            <version_patch>0</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/filter_nbsp</release_link>
            <download_link>https://bitbucket.org/hamuchen/filter_nbsp/downloads/filter_nbsp-7.x-1.0.zip</download_link>
            <date>1454624189</date>
            <mdhash>3383116b7e30f3d09c54ea82f4163d11</mdhash>
            <filesize>1003</filesize>
        </release>
    </releases>
</project>
