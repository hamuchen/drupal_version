<?xml version='1.0' encoding='utf-8'?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
    <title>WIT Admin Theme</title>
    <short_name>wit_admin</short_name>

    <api_version>7.x</api_version>

    <recommended_major>1</recommended_major>
    <supported_majors>1</supported_majors>
    <default_major>1</default_major>

    <project_status>published</project_status>

    <link>https://bitbucket.org/hamuchen/wit_admin</link>

    <terms>
        <term>
            <name>Projects</name>
            <value>Themes</value>
        </term>
    </terms>

    <releases>
        <release>
            <name>wit_admin 7.x-1.3</name>
            <version>7.x-1.3</version>

            <version_major>1</version_major>
            <version_patch>3</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/wit_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/wit_admin/downloads/wit_admin-7.x-1.3.zip</download_link>
            <date>1415902798</date>
            <mdhash>39ff8080e21080a3810c2adcc9617ac6</mdhash>
            <filesize>43396</filesize>
        </release>

        <release>
            <name>wit_admin 7.x-1.2</name>
            <version>7.x-1.2</version>

            <version_major>1</version_major>
            <version_patch>2</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/wit_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/wit_admin/downloads/wit_admin-7.x-1.2.zip</download_link>
            <date>1412881126</date>
            <mdhash>4d545b2ee64c8888405d826a9f51a88d</mdhash>
            <filesize>42061</filesize>
        </release>

        <release>
            <name>wit_admin 7.x-1.1</name>
            <version>7.x-1.1</version>

            <version_major>1</version_major>
            <version_patch>1</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/wit_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/wit_admin/downloads/wit_admin-7.x-1.1.zip</download_link>
            <date>1401899510</date>
            <mdhash>3c05e1169759993ee9246a1e8b101843</mdhash>
            <filesize>43524</filesize>
        </release>

        <release>
            <name>wit_admin 7.x-1.0</name>
            <version>7.x-1.0</version>

            <version_major>1</version_major>
            <version_patch>0</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/wit_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/wit_admin/downloads/wit_admin-7.x-1.0.zip</download_link>
            <date>1397671122</date>
            <mdhash>b6585354b09d1e59c6555de5b025a5ea</mdhash>
            <filesize>43181</filesize>
        </release>
    </releases>
</project>
