<?xml version='1.0' encoding='utf-8'?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
    <title>GOD: GOD of Development</title>
    <short_name>god</short_name>

    <api_version>7.x</api_version>

    <recommended_major>1</recommended_major>
    <supported_majors>1</supported_majors>
    <default_major>1</default_major>

    <project_status>published</project_status>

    <link>https://bitbucket.org/hamuchen/god</link>

    <terms>
        <term>
            <name>Projects</name>
            <value>Modules</value>
        </term>
    </terms>

    <releases>
        <release>
            <name>god 7.x-1.3</name>
            <version>7.x-1.3</version>

            <version_major>1</version_major>
            <version_patch>3</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/god</release_link>
            <download_link>https://bitbucket.org/hamuchen/god/downloads/god-7.x-1.3.zip</download_link>
            <date>1579208319</date>
            <mdhash>15b8aa36134144524e1aaf5cdbf58e8a</mdhash>
            <filesize>8455</filesize>
        </release>

        <release>
            <name>god 7.x-1.2</name>
            <version>7.x-1.2</version>

            <version_major>1</version_major>
            <version_patch>2</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/god</release_link>
            <download_link>https://bitbucket.org/hamuchen/god/downloads/god-7.x-1.2.zip</download_link>
            <date>1485996511</date>
            <mdhash>54c161d59461162a35675a230a09eeda</mdhash>
            <filesize>6705</filesize>
        </release>

        <release>
            <name>god 7.x-1.1</name>
            <version>7.x-1.1</version>

            <version_major>1</version_major>
            <version_patch>1</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/god</release_link>
            <download_link>https://bitbucket.org/hamuchen/god/downloads/god-7.x-1.1.zip</download_link>
            <date>1475801198</date>
            <mdhash>1934477f82ac846c66f9f951ab95bec3</mdhash>
            <filesize>6584</filesize>
        </release>

        <release>
            <name>god 7.x-1.0</name>
            <version>7.x-1.0</version>

            <version_major>1</version_major>
            <version_patch>0</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/god</release_link>
            <download_link>https://bitbucket.org/hamuchen/god/downloads/god-7.x-1.0.zip</download_link>
            <date>1454624189</date>
            <mdhash>3383116b7e30f3d09c54ea82f4163d11</mdhash>
            <filesize>1003</filesize>
        </release>
    </releases>
</project>
