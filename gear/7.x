<?xml version='1.0' encoding='utf-8'?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
    <title>Gear</title>
    <short_name>gear</short_name>

    <api_version>7.x</api_version>

    <recommended_major>1</recommended_major>
    <supported_majors>1</supported_majors>
    <default_major>1</default_major>

    <project_status>published</project_status>

    <link>https://bitbucket.org/hamuchen/gear</link>

    <terms>
        <term>
            <name>Projects</name>
            <value>Modules</value>
        </term>
    </terms>

    <releases>
        <release>
            <name>gear 7.x-1.2</name>
            <version>7.x-1.2</version>

            <version_major>1</version_major>
            <version_patch>2</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/gear</release_link>
            <download_link>https://bitbucket.org/hamuchen/gear/downloads/gear-7.x-1.2.zip</download_link>
            <date>1400251801</date>
        </release>

        <release>
            <name>gear 7.x-1.1</name>
            <version>7.x-1.1</version>

            <version_major>1</version_major>
            <version_patch>1</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/gear</release_link>
            <download_link>https://bitbucket.org/hamuchen/gear/downloads/gear-7.x-1.1.zip</download_link>
            <date>1398709062</date>
        </release>

        <release>
            <name>gear 7.x-1.0</name>
            <version>7.x-1.0</version>

            <version_major>1</version_major>
            <version_patch>0</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/gear</release_link>
            <download_link>https://bitbucket.org/hamuchen/gear/downloads/gear-7.x-1.0.zip</download_link>
            <date>1397671122</date>
        </release>
    </releases>
</project>
