<?xml version='1.0' encoding='utf-8'?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
    <title>Bootstrap Admin Theme</title>
    <short_name>bs_admin</short_name>

    <api_version>7.x</api_version>

    <recommended_major>3</recommended_major>
    <supported_majors>3</supported_majors>
    <default_major>3</default_major>

    <project_status>published</project_status>

    <link>https://bitbucket.org/hamuchen/bs_admin</link>

    <terms>
        <term>
            <name>Projects</name>
            <value>Themes</value>
        </term>
    </terms>

    <releases>
        <release>
            <name>bs_admin 7.x-3.9</name>
            <version>7.x-3.9</version>

            <version_major>3</version_major>
            <version_patch>9</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.9.zip</download_link>
            <date>1511553186</date>
            <mdhash>951ac1707400d2c1e5713b8f06fa867d</mdhash>
            <filesize>824962</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.8</name>
            <version>7.x-3.8</version>

            <version_major>3</version_major>
            <version_patch>8</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.8.zip</download_link>
            <date>1489617054</date>
            <mdhash>c6d208ff7508acbef9d685bd67e0e0d3</mdhash>
            <filesize>825889</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.7</name>
            <version>7.x-3.7</version>

            <version_major>3</version_major>
            <version_patch>7</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.7.zip</download_link>
            <date>1481248858</date>
            <mdhash>038275e5d121db3f2afb14d181a2efe6</mdhash>
            <filesize>798016</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.6</name>
            <version>7.x-3.6</version>

            <version_major>3</version_major>
            <version_patch>6</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.6.zip</download_link>
            <date>1475193889</date>
            <mdhash>3737a7e99d215fe9c0885a6c6c98b7e2</mdhash>
            <filesize>741203</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.5</name>
            <version>7.x-3.5</version>

            <version_major>3</version_major>
            <version_patch>5</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.5.zip</download_link>
            <date>1466191934</date>
            <mdhash>f83e7fbf97922d8b1f759acdc80d33da</mdhash>
            <filesize>739835</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.4</name>
            <version>7.x-3.4</version>

            <version_major>3</version_major>
            <version_patch>4</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.4.zip</download_link>
            <date>1466046103</date>
            <mdhash>a42e237a5aa9fd92af68ba2499ba46d6</mdhash>
            <filesize>739822</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.3</name>
            <version>7.x-3.3</version>

            <version_major>3</version_major>
            <version_patch>3</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.3.zip</download_link>
            <date>1463179058</date>
            <mdhash>4266e18df9b5eec1cb100080788a1a07</mdhash>
            <filesize>730212</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.2</name>
            <version>7.x-3.2</version>

            <version_major>3</version_major>
            <version_patch>2</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.2.zip</download_link>
            <date>1454624189</date>
            <mdhash>a9a86fb46e517aebb8b8550728d7d446</mdhash>
            <filesize>725267</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.1</name>
            <version>7.x-3.1</version>

            <version_major>3</version_major>
            <version_patch>1</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.1.zip</download_link>
            <date>1449672298</date>
            <mdhash>fb002a9ed70ab71948998647ef9e2b51</mdhash>
            <filesize>712942</filesize>
        </release>

        <release>
            <name>bs_admin 7.x-3.0</name>
            <version>7.x-3.0</version>

            <version_major>3</version_major>
            <version_patch>0</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/bs_admin</release_link>
            <download_link>https://bitbucket.org/hamuchen/bs_admin/downloads/bs_admin-7.x-3.0.zip</download_link>
            <date>1438379717</date>
        </release>
    </releases>
</project>
