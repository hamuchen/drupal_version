<?xml version='1.0' encoding='utf-8'?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
    <title>Fontawesome File Icons</title>
    <short_name>fontawesome_fileicons</short_name>

    <api_version>7.x</api_version>

    <recommended_major>1</recommended_major>
    <supported_majors>1</supported_majors>
    <default_major>1</default_major>

    <project_status>published</project_status>

    <link>https://bitbucket.org/hamuchen/fontawesome_fileicons</link>

    <terms>
        <term>
            <name>Projects</name>
            <value>Modules</value>
        </term>
    </terms>

    <releases>
        <release>
            <name>fontawesome_fileicons 7.x-1.0</name>
            <version>7.x-1.0</version>

            <version_major>1</version_major>
            <version_patch>0</version_patch>

            <status>published</status>

            <release_link>https://bitbucket.org/hamuchen/fontawesome_fileicons</release_link>
            <download_link>https://bitbucket.org/hamuchen/fontawesome_fileicons/downloads/fontawesome_fileicons-7.x-1.0.zip</download_link>
            <date>1447776939</date>
        </release>
    </releases>
</project>
